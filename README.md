# <img src="https://gitlab.rrze.fau.de/uploads/-/system/project/avatar/1800/ASVj.png?width=64" alt="drawing" /> ASVj - Jamf School ASV provider

ASVj helps schools and their administrators to export users and classes from ASV and convert the records for a Jamf School import. A solution to import records into Jamf School without a LDAP / AD or external cloud identity provider such as Azure.

ASVj is developed by a independed team in there free time. The FAUmac team helps provide the tool, so schools or other institutions can use it. The developers or the FAUmac are not asociated with ISB-AG in any manner - we just want to help schools and their admins. 

## How to use ASVj
*All versions with **_bash** are regular shell script.*  

1. **Dependencies for the ASVj bash scripts**

| package | version | needed for ASVj version |
| --- | --- | --- |
|cat|>= 8.30|v1.2 (bash)|
|grep|>= 3.4|v1.2 (bash)|
|sed|>= 4.7|v1.2 (bash)|
|awk|>= 5.0.1|v1.2 (bash)|

2. **Prepare your ASV**  
  * REDIST/Plugin/ASVj - Lehrer/Schüler.exf 
  * Datei > Verwaltung > Exportformate > Modulbezogene Funktionen 

3. **Add info not provided by ASV**  

| file | content | 
| ------ | ------ |
| dsgvo_exludes.csv | users (by ASV-ID) who prefere paper instead of jamf |
| admins.csv    | admin/teacher users for jamf who **don't** appear in ASV |
| test.csv    | test users for jamf who **don't** appear in ASV |

4. **Place ASV exports in `$ASV_DIR`**
5. **Place CSV from last run in `$INPUT/$CURRENT_CSV`**
6. **Run `bash ./asvj.sh`** (Follow instructions on STDOUT - if any)

**Hint:** **dsgvo_buddy.sh** helps adding entries to exclude specific users

## How to import the ASVj generated files

1. **Upload CSV file via Jamf School Import**
   1. Log in to your Jamf School instance
   2. Go to *"Organsiation" > "Import / Syncronize"*
   3. Select *"Import users from CSV"*
   4. Select your ASVj generated csv file and start the import via "Start Import". It's not necessary to edit the default settings.

2. **Start Apple School Manager (ASM) sync** (if configured and needed)   
If you have disabled the ASM auto sync, you have to start the process manually. To verify your settings go to "Organisation" > "Settings" > "Apple School Manager SFTP". Furthermore, the sync process can be triggerd there. If the sync isn't configured at all, visit the [Jamf School documentation](https://docs.jamf.com/jamf-school/documentation/Syncing_with_Apple_School_Manager.html).

## FAQ
1) This tool is free and can be used by anyone, but please note that we can not provide any support. Use this tool on your own risk.
2) **dsgvo_buddy.sh** helps adding entries to exclude specific users. So if students or parents have not consented to the data transfer you're able to exclude those users easily.
3) To easily remove old users in Jamf, the last imported ASVj file is needed (see input/old-jamf.csv). So it's **highly recommended to keep the last converted CSV file on a secure place**.

## 🚀 Planned features
* port to python3 (#1)

## Contact
[Create an GitLab issue](rrze-gitlab+faumac-asvj-1800-issue-@fau.de)

## ❤️ Special thanks
Special thanks to our main contributor **Bastian Söllmann** and **[FAUmac](https://gitlab.rrze.fau.de/faumac)** for providing this repository.
