#!/bin/bash
##
##
## DSGVO Tools
##
## dsgvo_buddy.sh
##
##
#

#declare variables

KEYWORD="ERROR"
MATCH="ERROR"
DB="./latest_export/schueler.csv"
EXCLUDES="./input/dsgvo_excludes.csv"
we_lucky="x"
looper="x"

#Search

while [ $looper != "exit" ]
do
	clear
	read -p "Searching your keyword in Database $DB " KEYWORD

	# Full LineOut for Display
	echo ""
	echo -n ">>> "
	grep -m1 $KEYWORD $DB

	# Safe ASV-ID into MATCH
	MATCH=$(grep -m1 $KEYWORD $DB | sed s/,.*//)

	#Process
	echo ""
	echo ""
	read -p "Wanna add $MATCH to $EXCLUDES? [y/n]? " we_lucky

	if [[ $we_lucky = "y" ]]
		then
		echo ""
		echo "Adding $MATCH to $EXCLUDES..."
		echo $MATCH >> $EXCLUDES
	else
		echo ""
		echo "Not adding $MATCH..."
	fi

	#Looper
	echo ""
	echo ""
	read -p "Add more [y/exit]? " looper

done

#Post-Stuff
echo ""
echo ""
echo "This is current DSGVO exclude list:"
echo ""
cat $EXCLUDES
exit
