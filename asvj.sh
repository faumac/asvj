#!/bin/bash
##
##
##
#
#


################
##  Variables ##
################


# vars
STUDENT_PATTERN="*schueler*"
TEACHER_PATTERN="*lehrer*"
CURRENT_CSV="old-jamf.csv"
DSGVO_EXCLUDES="dsgvo_excludes.csv"
ADMINS="admins.csv"
TEST="test.csv"
INPUT_DIR="./input"
ASV_DIR="./latest_export"
OUTPUT_DIR="./output"
WORKING_DIR="./tmp"
DOMAIN="schule.test"
MYCLASSES="ERROR"



#############
## Fancyfy ##
#############


GREEN="\e[00;32m"
YELLOW="\e[00;33m"
RED="\e[00;31m"
CYAN="\e[0;36m"
RESET="\e[00m"

function loginfo() { echo -e "$GREEN$1$RESET"; }
function logwarn() { echo -e "$YELLOW$1$RESET"; }
function logdebug() { echo -e "$CYAN$1$RESET"; }
function logerror() { echo -e "$RED$1$RESET"; }




##################
##  Magic stuff ##
##################


## Cleanup
rm -f $WORKING_DIR/*
rm -f $OUTPUT_DIR/*
loginfo "Cleanup DONE."


## Preparing files (merge if multiple files & remove duplicated entries)
cat  $ASV_DIR/$STUDENT_PATTERN | awk '!seen[$0]++' > $WORKING_DIR/students.csv
cat  $ASV_DIR/$TEACHER_PATTERN | awk '!seen[$0]++' > $WORKING_DIR/teachers.csv
loginfo "Preparing ASV files DONE."


## Fixing mail consistency
awk --field-separator=";" 'BEGIN { OFS=";" } {gsub("ö","oe",$4)}1' $WORKING_DIR/students.csv | \
awk --field-separator=";" 'BEGIN { OFS=";" } {gsub("Ö","Oe",$4)}1' | \
awk --field-separator=";" 'BEGIN { OFS=";" } {gsub("ä","ae",$4)}1' | \
awk --field-separator=";" 'BEGIN { OFS=";" } {gsub("Ä","Ae",$4)}1' | \
awk --field-separator=";" 'BEGIN { OFS=";" } {gsub("ü","ue",$4)}1' | \
awk --field-separator=";" 'BEGIN { OFS=";" } {gsub("Ü","Ue",$4)}1' | \
awk --field-separator=";" 'BEGIN { OFS=";" } {gsub("ß","ss",$4)}1' | \
awk --field-separator=";" 'BEGIN { OFS=";" } {gsub(" ","-",$4)}1' > $WORKING_DIR/$DOMAIN-jamf.csv

awk --field-separator=";" 'BEGIN { OFS=";" } {gsub("ö","oe",$4)}1' $WORKING_DIR/teachers.csv | \
awk --field-separator=";" 'BEGIN { OFS=";" } {gsub("Ö","Oe",$4)}1' | \
awk --field-separator=";" 'BEGIN { OFS=";" } {gsub("ä","ae",$4)}1' | \
awk --field-separator=";" 'BEGIN { OFS=";" } {gsub("Ä","Ae",$4)}1' | \
awk --field-separator=";" 'BEGIN { OFS=";" } {gsub("ü","ue",$4)}1' | \
awk --field-separator=";" 'BEGIN { OFS=";" } {gsub("Ü","Ue",$4)}1' | \
awk --field-separator=";" 'BEGIN { OFS=";" } {gsub("ß","ss",$4)}1' | \
awk --field-separator=";" 'BEGIN { OFS=";" } {gsub(" ","-",$4)}1' >> $WORKING_DIR/$DOMAIN-jamf.csv
loginfo "Fixing mail consistency DONE."


## Building class list
MYCLASSES=$(sed "s/.*@DOMAIN//g" $WORKING_DIR/students.csv | sed "s/;//g" | awk '!seen[$0]++' | sort | sed ':a;N;$!ba;s/\n/,/g')",TEST"
loginfo "Building class list DONE."


## Writing Header
echo "Username;FirstName;LastName;Email;Groups;TeacherGroups;Password;Trashed" > $OUTPUT_DIR/$DOMAIN-jamf.csv
loginfo "Writing Header DONE."


## Merging database with manually created files
cat $INPUT_DIR/$ADMINS >> $WORKING_DIR/$DOMAIN-jamf.csv
cat $INPUT_DIR/$TEST >> $WORKING_DIR/$DOMAIN-jamf.csv
loginfo "Merging database DONE."


## DSGVO cleanup
grep --invert-match -f $INPUT_DIR/$DSGVO_EXCLUDES $WORKING_DIR/$DOMAIN-jamf.csv > $WORKING_DIR/$DOMAIN-jamf_cleaned.csv && cp $WORKING_DIR/$DOMAIN-jamf_cleaned.csv $WORKING_DIR/$DOMAIN-jamf.csv
loginfo "DSGVO cleaning DONE."


## Trashing
sed 's/;.*//' $INPUT_DIR/$CURRENT_CSV > $WORKING_DIR/current_ids.csv
sed 's/;.*//' $WORKING_DIR/$DOMAIN-jamf.csv > $WORKING_DIR/new_ids.csv
grep --invert-match -f $WORKING_DIR/new_ids.csv $WORKING_DIR/current_ids.csv > $WORKING_DIR/trashing_ids.csv

# Flag datasets (with Trash yes or no) that have or haven't disappeared or opt-out (DSGVO)
while read line; do
        DATASET=$line
        Entry=$(grep "$DATASET" $INPUT_DIR/$CURRENT_CSV)
        State=$(if grep -q -f $WORKING_DIR/trashing_ids.csv <<< "$DATASET"; then echo -n ';yes'; else echo -n ';no'; fi)
        echo -n "$Entry" >> $WORKING_DIR/current_csv_with_state.csv
        echo "$State" >> $WORKING_DIR/current_csv_with_state.csv
done < $INPUT_DIR/$CURRENT_CSV;

# Flag datasets (with Trash no) that have been found in latest export
while read line; do
        DATASET=$line
        Entry=$(grep "$DATASET" $WORKING_DIR/$DOMAIN-jamf.csv)
        State=$(echo -n ';no')
        echo -n "$Entry" >> $WORKING_DIR/$DOMAIN-jamf_with_state.csv
        echo "$State" >> $WORKING_DIR/$DOMAIN-jamf_with_state.csv
done < $WORKING_DIR/$DOMAIN-jamf.csv;
loginfo "Trashing DONE."


## Output
cat $WORKING_DIR/$DOMAIN-jamf_with_state.csv >> $OUTPUT_DIR/$DOMAIN-jamf.csv

if grep -q "\$" $WORKING_DIR/trashing_ids.csv
then
	grep "yes" $WORKING_DIR/current_csv_with_state.csv >> $OUTPUT_DIR/$DOMAIN-jamf.csv
else
logdebug "This seems to be our first run ever and no one opt-out -> no datasets to trash!"
fi
loginfo "Writing Output Done."


## replacing placeholders
sed -i "s/DOMAIN/$DOMAIN/g" $OUTPUT_DIR/$DOMAIN-jamf.csv
sed -i "s/MEINEKLASSEN/$MYCLASSES/g" $OUTPUT_DIR/$DOMAIN-jamf.csv
loginfo "[End] Replacing placeholders DONE."

exit
